import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.emojis = [];
    this.banana = "🍌";
    this.emit(Application.events.READY);
  }
  setEmojis(emojis) {
    this.emojis = emojis;

    console.log(emojis);
    
    // Clearing the content of the wrapper div
    const clearContent = document.getElementById('emojis');
    clearContent.replaceChildren();

      const newP = document.createElement("p");
      newP.textContent = emojis.join('');
      const element = document.getElementById("emojis");
      element.appendChild(newP);
    

  }

  addBananas() {
    let newArr = this.emojis.map(x => x + this.banana);

    this.setEmojis(newArr);

  }
}
